package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This class implements a GUI for a bidirectional stopwatch, it works with multiple threads.
 * Two threads work concurrently: EDT which deals with the GI and a new Thread which deals with counting. 
 */
public class ConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JPanel panel = new JPanel();
    private final JLabel display = new JLabel();
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final JButton stop = new JButton("stop");
    private final Agent agent = new Agent();

    /**
     * The constructor builds the GUI and (ugly) starts a new Thread for the count.
     */
    public ConcurrentGUI() {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);

        /*
         * The class should not start the thread, ugly
         */
        new Thread(agent).start();

        up.addActionListener(e -> agent.up());
        down.addActionListener(e -> agent.down());
        stop.addActionListener(e -> agent.stopCounting());
    }

    /**
     * Getter method.
     * This method exist just for classes which extends this (see reactiveGUI03)
     * @return the Agent which deals with the Count
     */
    protected Agent getAgent() {
        return this.agent;
    }

    /**
     * This class deals with counting and notifies the EDT if something changes.
     * Realistically every 100 millis it invoke the EDT in order to change the textfield which shows the count.
     */
    protected class Agent implements Runnable {

        private volatile boolean goingUp = true;
        private volatile boolean stop;
        private int count;

        @Override
        public void run() {
            while (!stop) {
                try {
                    SwingUtilities.invokeLater(() -> ConcurrentGUI.this.display.setText(String.valueOf(count)));
                    if (goingUp) {
                        count++;
                    } else {
                        count--;
                    }
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * sets an ascending order.
         */
        public void up() {
            this.goingUp = true;
        }

        /**
         * sets a descenging order.
         */
        public void down() {
            this.goingUp = false;
        }

        /**
         * stops the count and disable the buttons.
         * Realistically calls the EDT to do so.
         */
        public synchronized void stopCounting() {
            this.stop = true;
                try {
                    SwingUtilities.invokeAndWait(() -> Arrays.asList(ConcurrentGUI.this.panel.getComponents())
                                .stream().forEach(c -> c.setEnabled(false)));
                } catch (InvocationTargetException | InterruptedException e) {
                    e.printStackTrace();
                }
        }
    }

}
