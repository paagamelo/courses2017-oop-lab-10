package it.unibo.oop.lab.reactivegui03;

/**
 * This class implements a GUI for a bidirectional stopwatch.
 * The brand new feature: after 10 seconds the count stops and buttons are disabled.
 */
public class AnotherConcurrentGUI extends it.unibo.oop.lab.reactivegui02.ConcurrentGUI {

    private static final long serialVersionUID = 1L;
    private static final int MILLISECONDS_TO_WAIT = 10000;

    /**
     * The constructor start the new thread which, after sometime, will stop the count.
     */
    public AnotherConcurrentGUI() {
        super();
        final Stopper stopper = new Stopper();
        new Thread(stopper).start();
    }

    /*
     * This class deals with stopping the count after 10 sec.
     * To do so, it calls stopCounting() method from the Agent previously implemented (see ConcurrentGUI02)
     */
    private class Stopper implements Runnable {
        public void run() {
            try {
                Thread.sleep(MILLISECONDS_TO_WAIT);
                AnotherConcurrentGUI.this.getAgent().stopCounting();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
