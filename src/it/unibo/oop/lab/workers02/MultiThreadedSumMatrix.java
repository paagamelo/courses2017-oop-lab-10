package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class implements the multi-threaded sum of the elements of a matrix.
 */
public class MultiThreadedSumMatrix implements SumMatrix {

    private final int nthread;

    /**
     * @param nThreads
     *              the number of threads that will perform the sum
     */
    public MultiThreadedSumMatrix(final int nThreads) {
        this.nthread = nThreads;
    }

    /**
     * Sums the elements of a matrix provided as input.
     * @param matrix
     *              the matrix to sum
     * @return
     *              the sum
     *
     * It creates and starts a nThread number of threads and collect the result.
     */
    public double sum(final double[][] matrix) {
        final int matrixSize = matrix.length * matrix[0].length;
        final int size = matrixSize / nthread;

        final List<Worker> workers = IntStream.iterate(0, start -> start + size)
                .limit(nthread)
                .mapToObj(start -> new Worker(matrix, start, size))
                .collect(Collectors.toList());
        workers.add(new Worker(matrix, nthread * size, matrixSize % nthread));

        workers.forEach(Thread::start);

        workers.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        return workers.stream().mapToLong(Worker::getResult).sum();
    }

    /*
     * Deals with counting a number (nElem) of elements of the matrix.
     */
    private class Worker extends Thread {

        private final double[][] matrix;
        private final int nelem;
        private int indxRow;
        private int indxCol;
        private long res;

        /*
         * The starting position is not a pair of indexs but a single number,
         * this because (assumed a way to order a matrix's elements) it is possible to find row and column indxs
         * from a single number.
         */
        Worker(final double[][] matrix, final int startpos, final int nelem) {
            super();
            this.matrix = Objects.requireNonNull(matrix);
            this.nelem = nelem;
            this.indxCol = startpos % matrix[0].length;
            this.indxRow = startpos / matrix[0].length;
        }

        /*
         * Sums the elements of the matrix in "res" using streams.
         */
        @Override
        public void run() {
            this.res = IntStream.range(0, nelem)
                    .mapToLong(i -> (long) getElem())
                    .sum();
        }

        public long getResult() {
            return this.res;
        }

        /*
         * Gets the next element of the matrix
         * If the end of a line is reached the index of the column (indxCol) is reset and the index of row
         * (indxRow) is incremented. Otherwise just the indxCol is incremented. 
         * 
         * This could use a param indx, but i just don't have time to write it
         */
        private double getElem() {
            final double res = matrix[indxRow][indxCol];
            if (indxCol == matrix[0].length - 1) {
                indxRow++;
                indxCol = 0;
            } else {
                indxCol++;
            } 
            return res;
        }

    }

}
